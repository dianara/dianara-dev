/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QWidget>
#include <QIcon>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QStackedWidget>
#include <QTabWidget>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QSettings>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

#include <QDebug>

#include "globalobject.h"
#include "fontpicker.h"
#include "colorpicker.h"
#include "notifications.h"
#include "proxydialog.h"


class ConfigDialog : public QWidget
{
    Q_OBJECT

public:
    ConfigDialog(GlobalObject *globalObject,
                 QString dataDirectory,
                 int updateInterval,
                 int tabsPosition,
                 bool tabsMovable,
                 FDNotifications *notifier,
                 QWidget *parent);
    ~ConfigDialog();

    void createGeneralPage(int updateInterval,
                           int tabsPosition,
                           bool tabsMovable);
    void createFontsPage();
    void createColorsPage();
    void createTimelinesPage();
    void createPostsPage();
    void createComposerPage();
    void createPrivacyPage();
    void createNotificationsPage(QSettings *settings);
    void createSystrayPage();

    QComboBox *newAvatarComboBox();

    void syncNotifierOptions();
    QString checkNotifications(int notificationStyle);
    void setPublicPosts(bool value);


signals:
    void configurationChanged();
    void filterEditorRequested();


public slots:
    void saveConfiguration();
    void pickCustomIconFile();
    void showDemoNotification(int notificationStyle);
    void toggleNotificationDetails(int currentOption);
    void toggleCustomIconButton(int currentOption);


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_topLayout;


    QListWidget *m_categoriesListWidget;
    QStackedWidget *m_categoriesStackedWidget;


    // Page 1, general options
    QWidget *m_generalOptionsWidget;
    QFormLayout *m_generalOptionsLayout;

    QSpinBox *m_updateIntervalSpinbox;
    QComboBox *m_tabsPositionCombobox;
    QCheckBox *m_tabsMovableCheckbox;

    QPushButton *m_proxyButton;
    ProxyDialog *m_proxyDialog;

    QPushButton *m_filterEditorButton;


    // Page 2, fonts
    QWidget *m_fontOptionsWidget;
    QVBoxLayout *m_fontOptionsLayout;

    FontPicker *m_fontPicker1;
    FontPicker *m_fontPicker2;
    FontPicker *m_fontPicker3;
    FontPicker *m_fontPicker4;


    // Page 3, colors
    QWidget *m_colorOptionsWidget;
    QVBoxLayout *m_colorOptionsLayout;

    ColorPicker *m_colorPicker1;
    ColorPicker *m_colorPicker2;
    ColorPicker *m_colorPicker3;
    ColorPicker *m_colorPicker4;
    ColorPicker *m_colorPicker5;
    ColorPicker *m_colorPicker6;


    // Page 4, timelines options
    QWidget *m_timelinesOptionsWidget;
    QFormLayout *m_timelinesOptionsLayout;

    QSpinBox *m_postsPerPageMainSpinbox;
    QSpinBox *m_postsPerPageOtherSpinbox;

    QCheckBox *m_showDeletedCheckbox;
    QCheckBox *m_hideDuplicatesCheckbox;
    QCheckBox *m_jumpToNewCheckbox;

    QComboBox *m_mfSnippetsCombobox;
    QSpinBox *m_snippetLimitSpinbox;
    QSpinBox *m_snippetLimitHlSpinbox;
    QComboBox *m_mfAvatarSizeCombobox;
    QComboBox *m_mfIconTypeCombobox;


    // Page 5, posts options
    QWidget *m_postsOptionsWidget;
    QFormLayout *m_postsOptionsLayout;

    QComboBox *m_postAvatarSizeCombobox;
    QComboBox *m_commentAvatarSizeCombobox;
    QCheckBox *m_showExtendedSharesCheckbox;
    QCheckBox *m_showExtraInfoCheckbox;
    QCheckBox *m_postHlAuthorCommentsCheckbox;
    QCheckBox *m_postHLOwnCommentsCheckbox;

    QCheckBox *m_postIgnoreSslInImages;
    QCheckBox *m_postFullImagesCheckbox;


    // Page 6, composer options
    QWidget *m_composerOptionsWidget;
    QFormLayout *m_composerOptionsLayout;

    QCheckBox *m_publicPostsCheckbox;
    QCheckBox *m_useFilenameAsTitleCheckbox;
    QCheckBox *m_showCharacterCounterCheckbox;



    // Page 7, privacy options
    QWidget *m_privacyOptionsWidget;
    QFormLayout *m_privacyOptionsLayout;

    QCheckBox *m_silentFollowsCheckbox;
    QCheckBox *m_silentListsCheckbox;
    QCheckBox *m_silentLikesCheckbox;



    // Page 8, notifications options
    QWidget *m_notifOptionsWidget;
    QFormLayout *m_notifOptionsLayout;

    QComboBox *m_notifStyleCombobox;
    QSpinBox *m_notifDurationSpinbox;
    QCheckBox *m_notifPersistentCheckbox;
    QCheckBox *m_notifTaskbarCheckbox;
    QLabel *m_notifStatusLabel;
    QCheckBox *m_notifyNewTLCheckbox;
    QCheckBox *m_notifyHLTLCheckbox;
    QCheckBox *m_notifyNewMWCheckbox;
    QCheckBox *m_notifyHLMWCheckbox;
    QCheckBox *m_notifyErrorsCheckbox;


    // Page 9, system tray options
    QWidget *m_systrayOptionsWidget;
    QFormLayout *m_systrayOptionsLayout;

    QComboBox *m_systrayIconTypeCombobox;
    QPushButton *m_systrayCustomIconButton;
    QString m_systrayCustomIconFN;
    QString m_systrayIconLastDir;
    QCheckBox *m_systrayHideCheckbox;


    ///////////////////////////////////////////////////////////////////////

    // Widgets below the stack widget
    QLabel *m_dataDirectoryLabel;

    QDialogButtonBox *m_buttonBox;
    QPushButton *m_saveConfigButton;
    QPushButton *m_cancelButton;

    QAction *m_closeAction;

    FDNotifications *m_fdNotifier;
    GlobalObject *m_globalObject;
};

#endif // CONFIGDIALOG_H
