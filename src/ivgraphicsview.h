#ifndef IVGRAPHICSVIEW_H
#define IVGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>

#include <QDebug>


class IvGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit IvGraphicsView(QGraphicsScene *scene, QWidget *parent = 0);
    ~IvGraphicsView();

    double getScaleToFit(int width, int height, int angle);

signals:
    void zoomInRequested();
    void zoomOutRequested();

    void zoomableModeRequested();
    void doubleClicked();


public slots:

protected:
    virtual void wheelEvent(QWheelEvent *event);
    virtual void mouseDoubleClickEvent(QMouseEvent *event);


private:

};

#endif // IVGRAPHICSVIEW_H
