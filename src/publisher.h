/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PUBLISHER_H
#define PUBLISHER_H

#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QToolButton>
#include <QMenu>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressBar>

#include <QDebug>

#include "composer.h"
#include "pumpcontroller.h"
#include "globalobject.h"
#include "mischelpers.h"
#include "audienceselector.h"
#include "draftsmanager.h"


class Publisher : public QWidget
{
    Q_OBJECT

public:
    explicit Publisher(PumpController *pumpController,
                       GlobalObject *globalObject,
                       QWidget *parent = nullptr);
    ~Publisher();

    void syncFromConfig();
    void setEmptyMediaData();
    void setTitleAndContent(QString title, QString content);

    QVariantMap getAudienceMap();
    void setAudienceFromMap(QVariantMap audienceMap);

    void setMediaModeWidgets();
    void findMediaFile();
    void toggleWidgetsWhileSending(bool widgetsEnabled);

    bool isFullMode();
    bool emptyContents();

    void reportUnreadableFile(QString filename, QFileInfo fileInfo);


signals:


public slots:
    void setMinimumMode();
    void setFullMode();

    void setPictureMode();
    void setAudioMode();
    void setVideoMode();
    void setFileMode();
    void onFileDropped(QString fileUrl);

    void cancelMediaMode();

    void setEditingMode(QString postId, QString postType,
                        QString postTitle, QString postText);


    void startMessageForContact(QString id, QString name, QString url);
    void addNickToRecipients(QString id, QString name, QString url,
                             QString listType);


    void onDraftSelected(QString id,
                         QString title, QString body,
                         QString type, QString attachment,
                         QVariantMap audience, int position);
    void onSaveDraftRequested();
    void onCancelSavingDraftRequested();


    void onPublishingOk();
    void onPublishingFailed();

    void onToPublicSelected();
    void onToFollowersSelected();
    void onCcPublicSelected();
    void onCcFollowersSelected();

    void updateAudienceToLabels();
    void updateAudienceCcLabels();

    void updateListsMenus(QVariantList listsList);

    void showHighlightedUrl(QString url);

    void sendPost();

    void onSelectMediaFilePressed();
    void updateProgressBar(qint64 sent, qint64 total);
    void onUploadFinished();

    void updateCharacterCounter();


private:
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_titleLayout;
    QGridLayout *m_mediaLayout;
    QGridLayout *m_buttonsLayout;


    QLabel *m_titleLabel;
    QLineEdit *m_titleLineEdit;
    QLabel *m_mediaInfoLabel;
    QPushButton *m_selectMediaButton;
    QPushButton *m_removeMediaButton;
    QLabel *m_pictureLabel;
    QProgressBar *m_uploadProgressBar;


#ifdef HAVE_KCHARSELECT
    QToolButton *m_charPickerButton;
#endif

    QPushButton *m_toolsButton;

    DraftsManager *m_draftsManager;
    QMenu *m_draftsMenu;
    QPushButton *m_draftsButton;

    Composer *m_composerBox;


    AudienceSelector *m_toAudienceSelector;
    QPushButton *m_toSelectorButton;
    QMenu *m_toSelectorMenu;
    QLabel *m_toAudienceLabel;

    AudienceSelector *m_ccAudienceSelector;
    QPushButton *m_ccSelectorButton;
    QMenu *m_ccSelectorMenu;
    QLabel *m_ccAudienceLabel;

#ifdef GROUPSUPPORT
    QLabel *m_groupIdLabel;
    QLineEdit *m_groupIdLineEdit;
#endif


    QPushButton *m_addMediaButton;
    QMenu *m_addMediaMenu;
    QAction *m_addMediaImageAction;
    QAction *m_addMediaAudioAction;
    QAction *m_addMediaVideoAction;
    QAction *m_addMediaFileAction;

    QLabel *m_charCounterLabel;

    QLabel *m_statusInfoLabel;

    QPushButton *m_postButton;
    QPushButton *m_cancelButton;


    bool m_onlyToFollowers;

    QString m_mediaFilename;
    QString m_mediaContentType;
    QString m_lastUsedDirectory;
    QString m_postType;

    bool m_editingMode;
    QString m_editingPostId;

    bool m_fullMode;

    QNetworkReply *m_uploadNetworkReply;
    bool m_uploading;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // PUBLISHER_H
