/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMMENT_H
#define COMMENT_H

#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QVariantMap>
#include <QMessageBox>
#include <QEvent>
#include <QResizeEvent>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "mischelpers.h"
#include "timestamp.h"
#include "asobject.h"
#include "avatarbutton.h"
#include "hclabel.h"


class Comment : public QFrame
{
    Q_OBJECT

public:
    explicit Comment(PumpController *pumpController,
                     GlobalObject *globalObject,
                     ASObject *commentObject,
                     QWidget *parent = 0);
    ~Comment();

    void updateDataFromObject(ASObject *object);

    void fixLikeLabelText();
    void setLikesCount(int count, QVariantList namesVariantList);

    void setFuzzyTimestamps();
    void syncAvatarFollowState();

    void setCommentContents();
    void onResize();
    void getPendingImages();

    QString getObjectId();

    void setHint(QString color="");

    void setCommentDeleted(QString deletedTime);


signals:
    void commentQuoteRequested(QString content);
    void commentEditRequested(QString id, QString content);


public slots:
    void likeComment(QString clickedLink);
    void saveCommentSelectedText();
    void quoteComment();
    void editComment();
    void deleteComment();

    void showUrlInfo(QString url);

    void redrawImages(QString imageUrl);

protected:
    virtual void leaveEvent(QEvent *event);
    virtual void resizeEvent(QResizeEvent *event);


private:
    QHBoxLayout *m_mainLayout;
    QVBoxLayout *m_leftLayout;
    QVBoxLayout *m_rightLayout;
    QHBoxLayout *m_rightTopLayout;

    AvatarButton *m_avatarButton;
    QLabel *m_fullNameLabel;
    HClabel *m_timestampLabel;
    QLabel *m_contentLabel;
    HClabel *m_likesCountLabel;

    QWidget *m_hintWidget;

    QLabel *m_likeLabel;
    QLabel *m_quoteLabel;
    QLabel *m_editLabel;
    QLabel *m_deleteLabel;


    QString m_commentId;
    QString m_objectType;
    QString m_commentAuthorId;

    bool m_commentIsLiked;
    bool m_commentIsDeleted;
    bool m_commentHasImages;
    QString m_createdAt;
    QString m_updatedAt;
    QString m_commentOriginalText;
    QStringList m_pendingImagesList;

    QString m_commentSelectedText;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // COMMENT_H
