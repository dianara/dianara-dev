/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "fontpicker.h"

FontPicker::FontPicker(QString description,
                       QString initialFontString,
                       QWidget *parent) : QWidget(parent)
{
    QFont initialFont;
    initialFont.fromString(initialFontString);
    if (initialFont.family().isEmpty()) // Invalid, so use standard font
    {
        m_currentFont = QFont();
    }
    else // Valid, so use it
    {
        m_currentFont = initialFont;
    }


    m_descriptionLabel = new QLabel(description, this);
    m_descriptionLabel->setWordWrap(true);


    m_sampleLineEdit = new QLineEdit(this);
    m_sampleLineEdit->setReadOnly(true);
    updateFontSample();


    m_button = new QPushButton(QIcon::fromTheme("list-add-font",
                                                QIcon(":/images/list-add.png")),
                               tr("Change..."),
                               this);
    connect(m_button, &QAbstractButton::clicked,
            this, &FontPicker::selectFont);


    m_layout = new QHBoxLayout();
    m_layout->addWidget(m_descriptionLabel, 2);
    m_layout->addWidget(m_sampleLineEdit,   5);
    m_layout->addWidget(m_button,           1);
    this->setLayout(m_layout);

    qDebug() << "FontPicker created";
}

FontPicker::~FontPicker()
{
    qDebug() << "FontPicker destroyed";
}


void FontPicker::updateFontSample()
{
    m_sampleLineEdit->setFont(m_currentFont);
    m_sampleLineEdit->setText(m_currentFont.toString());
    m_sampleLineEdit->setCursorPosition(0);
}


QString FontPicker::getFontInfo()
{
    return m_currentFont.toString();
}


////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SLOTS /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void FontPicker::selectFont()
{
    bool fontOk = false;

    QFont newFont = QFontDialog::getFont(&fontOk,
                                         m_currentFont,
                                         this,
                                         tr("Choose a font"));
    if (fontOk)
    {
        m_currentFont = newFont;
        updateFontSample();
    }
}
