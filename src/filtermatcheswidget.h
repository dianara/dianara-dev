/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef FILTERMATCHESWIDGET_H
#define FILTERMATCHESWIDGET_H

#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>
#include <QVariantMap>

#include <QDebug>


class FilterMatchesWidget : public QFrame
{
    Q_OBJECT

public:
    explicit FilterMatchesWidget(QVariantMap filterMatchesMap,
                                 QWidget *parent = 0);
    ~FilterMatchesWidget();


    QString tagList(QStringList tagsList);


signals:


public slots:


private:
    QHBoxLayout *m_layout;
    QLabel *m_contentLabel;

};

#endif // FILTERMATCHESWIDGET_H
