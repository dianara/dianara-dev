/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONTACTCARD_H
#define CONTACTCARD_H

#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QString>
#include <QMap>
#include <QFont>
#include <QFile>
#include <QPushButton>
#include <QMenu>
#include <QMessageBox>

#include <QDebug>

#include "mischelpers.h"
#include "pumpcontroller.h"
#include "globalobject.h"
#include "asperson.h"
#include "timestamp.h"


class ContactCard : public QFrame
{
    Q_OBJECT

public:
    ContactCard(PumpController *pumpController,
                GlobalObject *globalObject,
                ASPerson *asPerson,
                QWidget *parent = 0);
    ~ContactCard();

    void setButtonToFollow();
    void setButtonToUnfollow();
    bool setAvatar(QString avatarFilename);

    QString getNameAndIdString();
    QString getId();

signals:

public slots:
    void followContact();
    void unfollowContact();

    void openProfileInBrowser();
    void setMessagingModeForContact();
    void browseContactPosts();

    void redrawAvatar(QString avatarUrl, QString avatarFilename);


private:
    QHBoxLayout *m_mainLayout;
    QVBoxLayout *m_rightLayout;

    QLabel *m_avatarLabel;

    QLabel *m_userInfoLabel;

    QPushButton *m_followButton;
    QPushButton *m_optionsButton;
    QMenu *m_optionsMenu;
    QAction *m_openProfileAction;
    QAction *m_sendMessageAction;
    QAction *m_browsePostsAction;
    //QMenu *m_addToListMenu;

    QString m_contactName;
    QString m_contactId;
    QString m_contactUrl;
    QString m_contactAvatarUrl;
    QString m_contactOutbox;

    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
};

#endif // CONTACTCARD_H
