/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef EMAILCHANGER_H
#define EMAILCHANGER_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QAction>
#include <QCloseEvent>

#include <QDebug>

#include "pumpcontroller.h"


class EmailChanger : public QWidget
{
    Q_OBJECT

public:
    explicit EmailChanger(QString explanation,
                          PumpController *pumpController,
                          QWidget *parent = 0);
    ~EmailChanger();

    void setCurrentEmail(QString email);


signals:


public slots:
    void validateFields();
    void changeEmail();
    void cancelDialog();


protected:
    virtual void closeEvent(QCloseEvent *event);


private:
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_middleLayout;
    QDialogButtonBox *m_bottomButtonBox;

    QLabel *m_infoLabel;

    QLineEdit *m_mailLineEdit;
    QLineEdit *m_mailRepeatLineEdit;
    QLineEdit *m_passwordLineEdit;

    QLabel *m_errorsLabel;

    QPushButton *m_changeButton;
    QPushButton *m_cancelButton;
    QAction *m_cancelAction;

    QString m_currentEmail;
    PumpController *m_pumpController;
};

#endif // EMAILCHANGER_H
