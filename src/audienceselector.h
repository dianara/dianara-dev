/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef AUDIENCESELECTOR_H
#define AUDIENCESELECTOR_H

#include <QFrame>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QGroupBox>
#include <QListWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QAction>
#include <QMenu>
#include <QCloseEvent>
#include <QHideEvent>
#include <QSettings>

#include <QDebug>

#include "pumpcontroller.h"
#include "peoplewidget.h"


class AudienceSelector : public QFrame
{
    Q_OBJECT

public:
    explicit AudienceSelector(PumpController *pumpController,
                              QString selectorType,
                              QWidget *parent = nullptr);
    ~AudienceSelector();

    void resetLists();
    void deletePrevious();
    void saveSelected();
    void restoreSelected();

    QMenu *getSelectorMenu();
    void setDefaultAudience(bool toPublic);
    void clearPublicAndFollowers();

    void setPublic(bool state);
    bool isPublicSelected();
    void setFollowers(bool state);
    bool isFollowersSelected();

    void setListsMenu(QVariantList newLists);
    void checkListWithId(QString id);

    QString updatedAudienceLabels();

    QVariantList getAudienceList(bool *onlyToFollowers);

    int getRecipientsCount();


signals:
    void audienceChanged();

    void publicSelected();
    void followersSelected();


public slots:
    void copyToSelected(QIcon contactIcon, QString contactString,
                        QString contactName, QString contactId,
                        QString contactUrl);
    void setAudience();

    void onListToggled(QAction *listAction);

    void onPublicToggled(bool checked);
    void onFollowersToggled(bool checked);


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);


private:
    QString m_selectorType;

    QVBoxLayout *m_mainLayout;

    QHBoxLayout *m_upperLayout;

    QVBoxLayout *m_allGroupboxLayout;
    QGroupBox *m_allContactsGroupbox;

    PeopleWidget *m_peopleWidget;

    QVBoxLayout *m_selectedGroupboxLayout;
    QGroupBox *m_selectedListGroupbox;

    QLabel *m_explanationLabel;
    QListWidget *m_selectedListWidget;

    QList<QListWidgetItem *> m_previousItems;

    QPushButton *m_clearSelectedListButton;

    QDialogButtonBox *m_bottomButtonBox;
    QPushButton *m_doneButton;
    QPushButton *m_cancelButton;


    QMenu *m_selectorMenu;
    QAction *m_publicAction;
    QAction *m_followersAction;
    QMenu *m_listsMenu;


    QAction *m_doneAction;
    QAction *m_cancelAction;

    PumpController *m_pumpController;
};

#endif // AUDIENCESELECTOR_H
