/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QWidget>
#include <QSettings>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QByteArray>
#include <QHideEvent>

#include <QDebug>

#include "pumpcontroller.h"


class AccountDialog : public QWidget
{
    Q_OBJECT

public:
    AccountDialog(PumpController *pumpController, QWidget *parent = 0);
    ~AccountDialog();
    void setLockMode(bool locked);

signals:
    void userIdChanged(QString newUserId);

public slots:
    void askForToken();
    void setVerifierCode();

    void onVerifierChanged(QString newText);

    void onAuthorizationSucceeded();
    void showAuthorizationStatus(bool authorized);
    void showAuthorizationUrl(QUrl url, bool browserLaunched);

    void saveDetails();

    void unlockDialog();


protected:
    virtual void hideEvent(QHideEvent *event);


private:
    QVBoxLayout *m_mainLayout;

    QLabel *m_help1Label;

    QHBoxLayout *m_idLayout;
    QLabel *m_userIdIconLabel;
    QLabel *m_userIdLabel;
    QLineEdit *m_userIdLineEdit;
    QPushButton *m_getVerifierButton;

    QFrame *separatorLine;

    QLabel *m_help2Label;

    QHBoxLayout *m_verifierLayout;
    QLabel *m_verifierIconLabel;
    QLabel *m_verifierLabel;
    QLineEdit *m_verifierLineEdit;
    QPushButton *m_authorizeButton;


    QLabel *m_errorsLabel;
    QLabel *m_authorizationStatusLabel;

    QLabel *m_unlockExplanationLabel;
    QPushButton *m_unlockButton;

    QDialogButtonBox *m_bottomButtonBox;
    QPushButton *m_saveButton;
    QPushButton *m_cancelButton;


    PumpController *m_pumpController;
};

#endif // ACCOUNT_H
