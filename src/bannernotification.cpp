/*
 *   This file is part of Dianara
 *   Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "bannernotification.h"


BannerNotification::BannerNotification(QWidget *parent) : QWidget(parent)
{
    /*
     * In the future, this widget might be used to notify different things.
     *
     * For now, it's only used to passively notify about pending autoupdates.
     *
     */

    m_iconLabel = new QLabel(this);
    m_iconLabel->setPixmap(QIcon::fromTheme("dialog-information",    // "clock", maybe?
                                            QIcon(":/images/feed-clock.png")) // FIXME: proper fallback
                           .pixmap(32, 32));

    m_descriptionLabel = new QLabel(tr("Timelines were not automatically "
                                       "updated to avoid interruptions."),
                                    this);
    m_descriptionLabel->setAlignment(Qt::AlignCenter);
    m_descriptionLabel->setWordWrap(true);
    // CSS for text color only; container widget has CSS for the background
    m_descriptionLabel->setStyleSheet("QLabel"
                                      "{ color: palette(highlighted-text); }");
    m_descriptionLabel->setToolTip("<b></b>"
                                   + tr("This happens when it is time to "
                                        "autoupdate the timelines, but you are "
                                        "not at the top of the first page, to "
                                        "avoid interruptions while you read"));


    m_okButton = new QPushButton(QIcon::fromTheme("view-refresh",
                                                  QIcon(":/images/menu-refresh.png")),
                                 tr("Update now"),
                                 this);
    connect(m_okButton, &QAbstractButton::clicked,
            this, &BannerNotification::onOk);

    m_cancelButton = new QPushButton(QIcon::fromTheme("window-close",
                                                      QIcon(":/images/button-close.png")),
                                     QString(),
                                     this);
    m_cancelButton->setToolTip("<b></b>"
                               + tr("Hide this message"));
    connect(m_cancelButton, &QAbstractButton::clicked,
            this, &BannerNotification::onCancel);


    m_containerLayout = new QHBoxLayout();
    m_containerLayout->addWidget(m_iconLabel);
    m_containerLayout->addWidget(m_descriptionLabel, 1);
    m_containerLayout->addWidget(m_okButton);
    m_containerLayout->addWidget(m_cancelButton);


    this->m_containerWidget = new QWidget(this);
    m_containerWidget->setObjectName("BannerWidget");
    m_containerWidget->setStyleSheet("QWidget#BannerWidget                    "
                                     "{ background-color: palette(highlight); "
                                     "  color: palette(highlighted-text);     "
                                     "  padding: 2px;                         "
                                     "  border-radius: 12px                   "
                                     "}");
    m_containerWidget->setLayout(m_containerLayout);


    m_mainLayout = new QHBoxLayout();
    m_mainLayout->setContentsMargins(0, 0, 0, 0);
    m_mainLayout->addWidget(m_containerWidget);
    this->setLayout(m_mainLayout);

    qDebug() << "BannerNotification created";
}


BannerNotification::~BannerNotification()
{
    qDebug() << "BannerNotification destroyed";
}


/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/


void BannerNotification::onOk()
{
    this->hide();

    emit updateRequested();
}


void BannerNotification::onCancel()
{
    this->hide();

    emit bannerCancelled();
}

