
 Dianara - A Pump.io client
 Copyright 2012-2024  JanKusanagi JRR <jancoding@gmx.com>

===============================================================================

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the
   Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.

   Or visit http://www.gnu.org/licenses/

===============================================================================


These are some basic instructions on how to build Dianara from source, in case
you don't have precompiled packages available for your operating system.
See https://jancoding.wordpress.com/dianara for info on package availability.



General runtime dependencies (check carefully!)
===============================================================================

 - Qt 5.6 or later
 - QOAuth 2.x, built with Qt 5
 - OpenSSL plugin for QCA (qca2-plugin-openssl, libqca2-plugin-ossl, or similar)
   In some cases, the QCA package includes the plugins directly.
    *** Dianara will _crash_ if you don't have this!
 - Optionally, KDE Sonnet for spellchecking. Debian users will need
   sonnet-plugins besides the main libkf5sonnet***5 packages.
 - Optionally, KDE KWidgetsAddons for the KCharSelect-based character picker.



Dependencies for building
===============================================================================

You'll need qmake and the qt-devel (>= 5.6, including QtNetwork and QtDBUS
modules, if they are separate), and qoauth-devel packages.
Qmake might be included in the Qt development packages, or it might be a
separate package.


These are the names of the packages for the build dependencies
in some GNU/Linux distributions:

  - Mageia (probably in Mandriva and ROSA, too):
      lib64qt5base5-devel and lib64qoauth-qt5-devel.
      *Optional: lib64kf5sonnet-devel, for spellchecking support.
      *Optional: lib64kf5widgetsaddons-devel (>=5.25), for symbol picker.

      ** Note: For the i586 architecture, remove the "64" from the package
               names, such as libqt5base5-devel.

    > Build tools: gcc-c++, make


  - Debian (probably in any of its derivatives, too):
      qt5-default and libqoauth-dev (>=2.0.0, or libqoauth-qt5-dev).
      *Optional: libkf5sonnet-dev, for spellchecking support.
      *Optional: libkf5widgetsaddons-dev (>=5.25), for symbol picker.

    > Build tools: build-essential, g++


  - Fedora:
      qt5-qtbase-devel, qoauth-qt5-devel.

    > Build tools: gcc-c++


  - openSUSE:
      libqt5-qtbase-devel and qoauth-qt5-devel.


  - Archlinux and Parabola (runtime deps include build-time deps):
      qt5-base and qoauth.
      *Optional: sonnet for spellchecking support.
      *Optional: kwidgetsaddons for symbol picker.

    > Build tools: gcc, make


  - FreeBSD (runtime deps include build-time deps):
      qt5-widgets and qoauth-qt5.

    > Build tools: qmake-qt5, qt5-buildtools


You might also need to install qt5-qmake, if your distribution does not include
it with the Qt development package.



Build process
===============================================================================

From Dianara's main directory, where Dianara.pro is located, execute:

        mkdir build               # Create a clean directory for the build
        cd build                  # Go into it
        qmake ..                  # Ask Qmake to generate a Makefile[*]
        make                      # Run Make to compile the project

[*]You might need to use the command 'qmake-qt5' instead
   If you have KF5 modules installed but want to build Dianara without support
   for those extra features, add CONFIG+=NO_KF5 to the qmake call.

That should do it!


There is an installation target if you wish to use 'make install', but you can
just run the resulting "dianara" binary without installation.
The language files will be embedded into the binary upon compilation,
so there's no need to keep them afterwards.

If you had built Dianara before, and have later installed the necessary
development packages for any of the optional features described in the
'General runtime dependencies' section, you should run 'make clean' on
the build directory before building again.


Dianara is built on and for GNU/linux, but it will probably work under other
systems, as long as they are supported by Qt, and have ports of the necessary
dependencies.



===============================================================================


Visit https://jancoding.wordpress.com/dianara for more information.

Get the latest development source from https://gitlab.com/dianara/dianara-dev
